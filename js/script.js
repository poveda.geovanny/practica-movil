

function searchMovie() {

    /// inclusion a uso de ajax
    $.ajax({  
    // Ajax tiene 4 parametros
    // la url del recurso que voy a consumir 
    url: "http://www.omdbapi.com",
    // tipo de metodo que voy a utilizar
    type: "get",
    // es como voy a procesar la informacion
    // consultada
    dataType: "json",
    data: {
        apikey: "4046b94d",
        s: $("#search-input").val()
      },

      // en result se almacena el
      // json que me retorna 
      // la informacion consumida

      success: function(result) {
 
        $.each(movies, function(i, data) {

            /// metodo append incluir el 
           /// resultado de la logica en el html
            $("#movie-list").append(
              `
              <div class="col-md-3">
              <div class="card mb-3">
            
              <!----- poster -->
              <img class="card-img-top" src=` +
            data.Poster +
            ` alt="Card image cap">


            <!----- titulo de la pelicula -->
                  <div class="card-body">
                      <h5 class="card-title">` +
            data.Title +
            `</h5>


            <!-----  año -->


                      <h6 class="card-subtitle mb-2 text-muted">` +
            data.Year +
            
            `</h6>

            <!-----  detalles -->

            <a href="#" class="card-link see-detail" data-toggle="modal" data-target="#exampleModal" data-id=` +
            data.imdbID +
            `>Detail</a>
                  </div>
              </div>
          </div>

              `
            );
          });
  
    })

}


$("#search-button").on("click", function() {
    searchMovie();
  });


  $("#movie-list").on("click", ".see-detail", 
  function() {


    
  }

  